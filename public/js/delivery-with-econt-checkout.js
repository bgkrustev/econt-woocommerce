jQuery( document ).ready( function (dwe) {  
  let use_shipping = false  
  /**
   * First we disable the "Enter" key 
   * because we need the "click" event in order to manage the flow
   */
  document.querySelector("form[name='checkout']").addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) { // 13 is enter
      e.preventDefault();
      e.stopPropagation();     
    }
  });      
  
  /**
   * We need this, because on update some fields, wordpress rerenders the html and breaks the listeners
   */
  dwe( document.body ).on( 'updated_checkout', function() {
    /**
     * The actual "click" event listener. It does:
     * 
     * 1. check if the selected method is our method and if it's not - submits the form.
     * WooCommerce is responsable to check all the required fields and manage the errors
     * 2. if the selected method is "Econt" makes an ajax call to obtain fresh URI from our plugin entry point
     * with the data provided by the user needed to display the Iframe
     */
    dwe( '#place_order' ).on( 'click', async function( e ){       
      if( dwe( '#ship-to-different-address-checkbox:checkbox:checked' )[0] ) {
        use_shipping = true
      } else {
        use_shipping = false
      }
           
      let selected_shipping_method = dwe( 'input[name^="shipping_method"]:checked' ).val()
      if ( selected_shipping_method === 'delivery_with_econt' && checkForm(use_shipping) ) {
        e.preventDefault()
        e.stopPropagation()

        let params = {}
        let post_data = {
          action: 'woocommerce_delivery_with_econt_get_orderinfo',
          security: delivery_with_econt_calculate_shipping_object.security,
        }
        
        let fName = '';
        if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_first_name' ) ) 
          fName = document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_first_name' ).value;
        let lName = '';
        if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_last_name' ) ) 
          lName = document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_last_name' ).value 
        params.customer_name = fName + ' ' + lName;
        if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_company' ) )
          params.customer_company = document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_company' ).value;
        if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_address_1' ) )
          params.customer_address = document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_address_1' ).value;
        if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_city' ) )
          params.customer_city_name = document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_city' ).value;
        if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_postcode' ) )
          params.customer_post_code = document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_postcode' ).value;
        if ( document.getElementById( 'billing_phone' ) )
          params.customer_phone = document.getElementById( 'billing_phone' ).value;
        if ( document.getElementById( 'billing_email' ) )
          params.customer_email = document.getElementById( 'billing_email' ).value
        post_data.params = params

        await dwe.ajax({
          type: 'POST',
          url: delivery_with_econt_calculate_shipping_object.ajax_url + '',
          data: post_data,
          success: function ( response ) {
            showIframe( response )
          },
          dataType: 'html'
        });               
      } else {
        dwe( '[name="checkout"]' ).submit()
      }
    });

  });

  /**
   * Event listener for the iframe window.
   * Handles the message sent back to us from Econt servers
   */
  window.addEventListener( 'message', function( message ) {
    // Данни връщани от формата за доставка:
    // id: уникално ID на адреса. Това поле трябва да бъде поставено в скритото customerInfo[id]
    // id_country: ID на държавата
    // zip: зип код на населеното място
    // post_code: пощенски код на населеното място
    // city_name: населено място
    // office_code: код на офиса на Еконт ако бъде избран такъв
    // address: адрес
    // name: име / фирма
    // face: лице
    // phone: телефон
    // email: имейл
    // shipping_price: цена на пратката без НП
    // shipping_price_cod: цена на пратката с НП
    // shipping_price_currency: валута на калкулираната цена
    // shipment_error: поясняващ текст ако е възникнала грешка

    let data = message['data']
    // Boolean stoper 
    let updateCart = false
    
    /**
     * възможно е да възникнат грешки 
     * при неправилно конфигурирани настройки на електронния магазин които пречат за калкулацията.
     * Here we print as a alert message any error returned from Econt.
     *  */
    if ( data['shipment_error'] && data['shipment_error'] !== '' ) {
      if ( confirm( 'Възникна следната грешка:' + "\r\n\r\n" + data['shipment_error'] + "\r\n\r\n" + 'Моля коригирайте формата или опитайте отново по-късно' ) ) {
        document.getElementById("myModal").click();
      }
      return false;
    }

    // Елемент от кода, където е указано дали стоката ще се заплаща с НП или не
    let codInput = document.getElementById('payment_method_cod');          

    // формата за калкулация връща цена с НП и такава без
    // спрямо избора на клиента в "Заплащане чрез НП" показваме правилната цена
    let shippmentPrice
    let confirmMessage 
    confirmMessage = "Цена на доставката: " + data['shipping_price'] + ' ' + data['shipping_price_currency'];    

    if ( codInput.checked ) {
      confirmMessage += " ( +" + (Math.round((data['shipping_price_cod'] - data['shipping_price']) * 100) / 100) + ' ' + data['shipping_price_currency'] + ' наложен платеж)';
      // confirmMessage += "\r\nОбща цена на куриерската услуга: " + data['shipping_price_cod'] + ' ' + data['shipping_price_currency'];    
      shippmentPrice = data['shipping_price_cod']
    } else  {
      // confirmMessage += "\r\nНаложен платеж: Без наложен платеж.";
      // confirmMessage += "\r\nОбща цена на куриерската услуга: " + data['shipping_price'] + ' ' + data['shipping_price_currency'];    
      shippmentPrice = data['shipping_price']
    }
    
    confirmMessage += "\r\nПотвърждавате ли покупката?";

    if ( confirm( confirmMessage ) ) {
      updateCart = true;    
    }
    
    if ( updateCart ) {
      /**
       * here we must:
       * 1. Update all fields if necesary;
       * 2. Populate all relevant hidden fields;
       * 3. Check if any other thing has to be done;
       * 4. Submit the form
       */

      /**
       * Set billing form fields
       */
      let full_name = []
      let company = ''
      if ( data['face'] != null ) {
        full_name = data['face'].split( ' ' );
        company = data['name'];
      } else {
        full_name = data['name'].split( ' ' );
      }
      if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_first_name' ) )
        document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_first_name' ).value = full_name[0];
      if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_last_name' ) )
        document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_last_name' ).value = full_name[1];
      if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_company' ) )
        document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_company' ).value = company;
      if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_address_1' ) )
        document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_address_1' ).value = data['address'] != '' ? data['address'] : data['office_name'];
      if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_city' ) )
        document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_city' ).value = data['city_name'];
      if ( document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_postcode' ) )
        document.getElementById( ( use_shipping ? 'shipping' : 'billing' ) + '_postcode' ).value = data['post_code'];
      if ( document.getElementById( 'billing_phone' ) )
        document.getElementById( 'billing_phone' ).value = data['phone'];
      if ( document.getElementById( 'billing_email' ) )
        document.getElementById( 'billing_email' ).value = data['email'];
      dwe( '#delivery_with_econt_customer_info_id' ).val( data['id'] )
      dwe( '#delivery_with_econt_shipping_price' ).val( shippmentPrice )
      // set all form fields to read-only, because we already have sent data to Econt
      dwe( "form[name='checkout'] :input:not(textarea)" ).each( function() {
        dwe(this).prop( 'readonly', true );
      });
      // Hide the modal
      document.getElementById("myModal").style.display = "none"
      // Triger WooCommerce update in order to populate the shipping price, the updated address field and if any other
      dwe( 'body' ).trigger( 'update_checkout' );
      // After updating the order data, submit the form.
      dwe( '[name="checkout"]' ).submit()
      // end          
    }
  }, false);  
});


/**
 * when press the big black "Place Order" button, this code will do:
 * 1. prevent the default behaviour;
 * 2. stop the propagation of the event;
 * 3. check the form for the required fields and display all the errors if any;
 * 4. open modal window with "Delivery with Econt" iframe, filled with the data
 */
function checkForm(use_shipping) {  
  let fields = [
    '#' + ( use_shipping ? 'shipping' : 'billing' ) + '_first_name',
    '#' + ( use_shipping ? 'shipping' : 'billing' ) + '_last_name',
    '#' + ( use_shipping ? 'shipping' : 'billing' ) + '_country',
    '#' + ( use_shipping ? 'shipping' : 'billing' ) + '_address_1',
    '#' + ( use_shipping ? 'shipping' : 'billing' ) + '_city',
    '#' + ( use_shipping ? 'shipping' : 'billing' ) + '_state',
    '#' + ( use_shipping ? 'shipping' : 'billing' ) + '_postcode',
    '#billing_phone',
    '#billing_email'
  ]      
  let showModal = true;

  fields.forEach( function( field ) {
    if( jQuery( field ).val() === '' ) { // check if field contains data
      showModal = false;
    }
  })

  return showModal;
}

/**
 * Render the actual iframe, nased on the provided user info
 *  
 * @param {data} data 
 */
function showIframe(data)
{
  let modal = document.getElementById("myModal");
  let span = document.getElementsByClassName("close")[0];
  let page = document.body

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
    page.style.overflowY = "auto"
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
      page.style.overflowY = "auto"
    }
  }
  let iframe
  iframe = '<iframe src="' + data.split( '"' ).join( '' ) + '" scrolling="yes" id="delivery_with_econt_iframe"></iframe>'
  // empty the div if any oter instances of the iframe were generated
  jQuery( '#place_iframe_here' ).empty();
  // append the generated iframe in the div
  jQuery( '#place_iframe_here' ).append(iframe);   
  // Show the modal
  modal.style.display = "block";
  page.style.overflowY = "hidden"
  
}
