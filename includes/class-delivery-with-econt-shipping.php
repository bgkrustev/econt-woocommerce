<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Delivery_With_Econt_Shipping extends WC_Shipping_Method
{
    // Shipping method title
    const TITLE = 'Econt';

    // Shipping method description
    const DESCRIPTION = 'Econt Shipping Method';
    // public $service_url;
    // public $demo_service_url;

    /**Wordpress shipping-related variables */
    public $supports;
    public $id;
    public $method_title;
    public $method_description;
    public $enabled;
    public $title;

    /**
     * Constructor for your shipping class
     *
     * @access public
     * @return void
     */
    public function __construct( $instance_id = 0 )
    {
        $this->id                 = Delivery_With_Econt_Options::get_plugin_name();
        $this->instance_id        = absint( $instance_id );
        $this->title              = isset($this->settings['title']) ? $this->settings['title'] : __(self::TITLE, 'delivery-with-econt');
        $this->method_title       = __(self::TITLE, 'delivery-with-econt');
        $this->method_description = __(self::DESCRIPTION, 'delivery-with-econt');
        $this->enabled            = isset($this->settings['enabled']) ? $this->settings['enabled'] : 'yes';
        $this->supports           = array(
			'shipping-zones',
			'instance-settings',
        );
        
        $this->init();
    }
    /**
    * Load the settings API
    */
    function init()
    {
        $this->init_form_fields();
        $this->init_settings();                
        // Save settings in admin if you have any defined
        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );        
    }    
    
    public function calculate_shipping( $package = [] )
    {       
        if ( $_POST['delivery_with_econt_customer_info_id'] === '' ) return;
        $cost = 0;
        
        if ( array_key_exists('delivery_with_econt_shipping_price', $_POST ) ) 
            $cost = $_POST['delivery_with_econt_shipping_price'];
        
            $rate = array(
            'id' => $this->id,
            'label' => $this->title,
            'cost' =>  $cost
        );

        $this->add_rate( $rate );
    }    
    
    /**
     * Тук можем да проверяваме кой е избраният метод за доставка и ако е Еконт, да проверяваме дали има свързаност със сърварите им.
     * Ако върне статус 500 - проблема е при тях.
     * Ако върне статус 400 - проблема е при нас.
     * Ако върне 200 - всичко е ОК и продължаваме.
     * 
     */

    public function validate_service()
    {
        return false;
    }

    // /**
    //  * Тук правим поръчка към Еконт
    //  * 
    //  * @todo remove
    //  */
    // public static function cteate_order($posted)
    // {
    //     $packages = WC()->shipping->get_packages();
    //     $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
    //     if ( is_array( $chosen_methods ) && in_array( 'delivery_with_econt', $chosen_methods ) ) {
    //         foreach ( $packages as $i => $package ) {
    //             if ( $chosen_methods[$i] != "delivery_with_econt" ) {
    //                 continue;
    //             }
    //             $cloudways_Shipping_Method = new Delivery_With_Econt_Shipping();
    //             $weightLimit = ( int ) $cloudways_Shipping_Method->settings['weight'];
    //             $weight = 1110;
    //             foreach ($package['contents'] as $item_id => $values) {
    //                 $_product = $values['data'];

    //                 $weight = $weight + intval($_product->get_weight()) * $values['quantity'];
    //             }
    //             $weight = wc_get_weight($weight, 'kg');
    //             if ($weight > $weightLimit) {
    //                 $message = sprintf(__('OOPS, %d kg increase the maximum weight of %d kg for %s', 'cloudways'), $weight, $weightLimit, $cloudways_Shipping_Method->title);
    //                 $messageType = "error";
    //                 if (!wc_has_notice($message, $messageType)) {
    //                     wc_add_notice($message, $messageType);
    //                 }
    //             }
    //         }
    //     }
    // }

    public static function get_order_info()
    {
        $url = DWEH()->get_service_url();

        $options = get_option( 'delivery_with_econt_settings' );

        $params = $_POST['params'];
        $order = array();
        // $order['total'] = round(floatval( preg_replace( '#[^\d.]#', '', WC()->cart->get_subtotal() ) ), 2);
        $order['order_total'] = WC()->cart->get_subtotal();
        $order['order_weight'] = WC()->cart->get_cart_contents_weight();
        $order['order_currency'] = get_woocommerce_currency();
        $order['id_shop'] = $options['store_id'];
        foreach ($params as $key => $value) {
            $order[$key] = $value;
        }
        $order['confirm_txt'] = 'Потвърди'; // текст за потвърждаващия бутон
        $order['ignore_history'] = 1; // изключване на автоматично попълване на полетата от историята
        wp_send_json($url . 'customer_info.php?' . http_build_query($order, null, '&'));
    }

    public static function render_form($checkout)
    {
        ?>
        <div id="delivery_with_econt_calculate_shipping">
            <input type="hidden" class="input-hidden" name="delivery_with_econt_customer_id" id="delivery_with_econt_customer_id" value="<?php echo WC()->checkout->get_value('delivery_with_econt_customer_id'); ?>">        
            <input type="hidden" name="delivery_with_econt_customer_info_id" id="delivery_with_econt_customer_info_id">
            <input type="hidden" name="delivery_with_econt_shipping_price" id="delivery_with_econt_shipping_price">
                
            <!-- ФОРМА ЗА ДОСТАВКА -->
            
            <!-- Modal -->
            <div id="myModal" class="modal">

                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <div class="modal-body" id="place_iframe_here"></div>
                </div>
            </div>
        </div>
        <?php
        
        wp_enqueue_style( 'delivery_with_econt_calculate_shipping', plugin_dir_url(__FILE__) . '../public/css/delivery-with-econt-checkout.css', [], false );
        wp_enqueue_script( 'delivery_with_econt_calculate_shipping', plugin_dir_url(__FILE__) . '../public/js/delivery-with-econt-checkout.js', ['jquery'], false, true );
        wp_localize_script( 'delivery_with_econt_calculate_shipping', 'delivery_with_econt_calculate_shipping_object', array('ajax_url' => admin_url('admin-ajax.php'), 'security'  => wp_create_nonce( 'delivery-with-econt-security-nonce' )));        
    }

}
